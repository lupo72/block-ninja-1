import {IS_TOUCH, playerInitState} from "../index";
import {Gamecontrols} from "../GameControls";

export class IntroScene extends Phaser.Scene {
    content;
    cursors;
    dynamic;
    i;
    j;
    p;


    constructor() {
        super({key: "IntroScene"});
    }

    init() {
        var controls = new Gamecontrols(this.input);
        this.cursors = controls.createCursorKeys();
        this.cameras.main.backgroundColor.setTo(128,255,255);
        // https://www.html5gamedevs.com/topic/41244-how-to-change-background-color/
        // this.cameras.main.backgroundColor = Phaser.Display.Color.HexStringToColor("#3498db");
    }

    create() {
        let width = this.cameras.main.width;
        let height = this.cameras.main.height;

        this.add.image(0, 120, 'front').setOrigin(0, 0).setScale(1.2);
        this.add.image(0, 120, 'IntroBg').setOrigin(0, 0);

        this.anims.create({key: 'Player-Idle', frames: [{key: 'Player', frame: 15}], frameRate: 10,});
        this.anims.create({key: 'Player-Climb-Idle', frames: [{key: 'Player', frame: 8}], frameRate: 10,});
        this.anims.create({
            key: 'Player-Walk',
            frames: this.anims.generateFrameNumbers('Player', {start: 0, end: 5}),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'Player-Run',
            frames: this.anims.generateFrameNumbers('Player', {start: 0, end: 5}),
            frameRate: 10,
            repeat: -1
        });
        this.anims.create({
            key: 'Player-Climb',
            frames: this.anims.generateFrameNumbers('Player', {start: 7, end: 8}),
            frameRate: 5,
            repeat: 100
        });
        this.anims.create({
            key: 'Opponent-Flame-Walk',
            frames: this.anims.generateFrameNumbers('Opponent-Flame', {start: 0, end: 2}),
            frameRate: 10,
            repeat: -1
        });


        let credits = [
            " !! Instructions: Arrow Keys to move or jump. Collect the keys to unlock the locks. ",
            "Pass through the door to advance to the next level. Avoid the Flames !! ",
            " ** Hand-Coded with Phaser 3 by Lupo ",
            "** Block Ninja 2D sprites by Korba ",
            "** Simple broad-purpose tileset by surt, sham & vk ",
            "** Prototyping 2D Pixelart Tilesets by robotality ",
            "** Forest Parallax by Robotrage ",
            "** 8-Bit Level Music: Vulnerability - Fast Version by Trevor Lentz ** ",
            "** Old-School-Font - by domsson ** ",
            "** Pixel Block Font - by J-Robot ** ",
        ];

        this.content = credits.join(' ');

        let textConfig = {
            image: 'BlockFont',
            width: 18,
            height: 32,
            chars: Phaser.GameObjects.RetroFont.TEXT_SET1,
            charsPerRow: 32
        };

        let textConfig2 = {
            image: 'oldschool',
            width: 7,
            height: 9,
            chars: Phaser.GameObjects.RetroFont.TEXT_SET1,
            charsPerRow: 18
        };

        // @ts-ignore
        this.cache.bitmapFont.add('oldschool', Phaser.GameObjects.RetroFont.Parse(this, textConfig2));
        this.dynamic = this.add.dynamicBitmapText(0, height - 20, 'oldschool', '+ + + + + + + + + + + + + + + + + + + + + + + + + + + + ');
        this.dynamic.setScale(2);

        // @ts-ignore
        this.cache.bitmapFont.add('BlockFont', Phaser.GameObjects.RetroFont.Parse(this, textConfig));
        this.add.dynamicBitmapText(10, 30, 'BlockFont', "Block Ninja Game Demo");
        let starttext0 = this.add.dynamicBitmapText(10, 80, 'BlockFont', "Hit");
        let starttext1 = this.add.dynamicBitmapText(80, 80, 'BlockFont', IS_TOUCH ? "Screen" : "Space");
        let starttext2 = this.add.dynamicBitmapText(220, 80, 'BlockFont', "to Start Game.");
        starttext1.setTint(0xff0000, 0xffff00, 0x00ffff, 0xffffff);
        this.tweens.add({
            targets: starttext1,
            alpha: 0.2,
            scaleY: 0.9,
            duration: 1200,
            ease: 'None',
            yoyo: true,
            repeat: -1
        });


        this.input.keyboard.on('keydown', function (key) {
            if (key.keyCode === 32)
            this.startGame();

        }, this);

        this.input.on('pointerdown', function (pointer) {
            this.startGame();
        }, this);

        this.i = 0;
        this.p = this.add.tileSprite(430, 430, 32, 32, 'Player', 5);
    }

    startGame()
    {
        if ( IS_TOUCH && window.innerHeight > window.innerWidth )
        {
            alert('Please Switch to Landscape Mode to Start Game!');
        }
        else
        {
            this.scene.stop("IntroScene");
            this.scene.start("GameScene", {level: 1, playerAttrs: playerInitState()});
        }
    }

    update(time, delta) {
        // https://labs.phaser.io/edit.html?src=src\game%20objects\bitmaptext\retro%20font\scrolling%20retro%20text.js
        // Scroller Speed
        this.dynamic.scrollX += 1.25;
        // equals width of a character
        if (this.dynamic.scrollX >= 7) {
            //  Remove first character
            let current = this.dynamic.text.substr(1);
            //  Add next character from the string
            current = current.concat(this.content[this.i]);
            this.i++;
            if (this.i === this.content.length) {

                this.i = 0;
            }
            //  Set it
            this.dynamic.setText(current);
            //  Reset scroller
            this.dynamic.scrollX = this.dynamic.scrollX % 7;
        }
    }
}
