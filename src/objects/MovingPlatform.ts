export class MovingPlatform extends Phaser.GameObjects.Sprite {

    velocityX;
    maxX;
    minY;
    minX;
    maxY;
    velocityY;

    constructor(gameScene, objekt, player, map) {

        var x = objekt.x;
        var y = objekt.y;
        var texture = 'Tiles';
        var frame = objekt.gid - 1;

        super(gameScene.physics.scene, x, y, texture, frame);

        gameScene.physics.scene.add.existing(this);
        gameScene.physics.add.existing(this);
        this.body.setAllowGravity(false);
        this.body.setImmovable(true);
        gameScene.physics.add.collider(player, this, player.hitObject, null, player);

        var props = objekt.properties;

        this.velocityY = props.hasOwnProperty('velocityY') ? parseInt(props.velocityY) : null;
        this.velocityX = props.hasOwnProperty('velocityX') ? parseInt(props.velocityX) : null;
        this.maxX = props.hasOwnProperty('maxX') ? parseInt(props.maxX) * map.tileWidth : null;
        this.minX = props.hasOwnProperty('minX') ? parseInt(props.minX) * map.tileWidth : null;
        this.maxY = props.hasOwnProperty('maxY') ? parseInt(props.maxY) * map.tileHeight : null;
        this.minY = props.hasOwnProperty('minY') ? parseInt(props.minY) * map.tileHeight : null;
    }

    update()
    {
        if (this.velocityX)
        {
            this.body.setVelocityX(this.velocityX);
            if (this.velocityX > 0 && this.body.x >= this.maxX) this.velocityX = -this.velocityX;
            if (this.velocityX < 0 && this.body.x <= this.minX) this.velocityX = -this.velocityX;

        }

        if (this.velocityY)
        {
            this.body.setVelocityY(this.velocityY);
            if (this.velocityY > 0 && this.body.y >= this.maxY) this.velocityY = -this.velocityY;
            if (this.velocityY < 0 && this.body.y <= this.minY) this.velocityY = -this.velocityY;
        }

    }

}
