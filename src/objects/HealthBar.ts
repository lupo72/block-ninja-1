import {HEALTH_STATES} from "../index";

export class HealthBar extends Phaser.GameObjects.Graphics {

    constructor(sceneObject) {
        super(sceneObject.physics.scene, null);
        this.setScrollFactor(0);
        sceneObject.physics.scene.add.existing(this);
    }

    update(health = 0)
    {
        let barColor = 0x00CC00;
        if (health < HEALTH_STATES.yellow ) barColor = 0xCCCC00;
        if (health < HEALTH_STATES.red ) barColor = 0xFF0000;
        this.clear();
        this.lineStyle(5, barColor, 5.0);
        this.beginPath();
        let barSize = health / 10;
        let barX = 0;
        let barY = 20;
        this.moveTo(barX , barY);
        this.lineTo(barX + barSize, barY);
        this.closePath();
        this.strokePath();
    }

}

