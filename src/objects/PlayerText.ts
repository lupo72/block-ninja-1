import {IS_TOUCH} from "../index";

export class PlayerText extends Phaser.GameObjects.Text {

    constructor(sceneObject) {
        let hudX = IS_TOUCH ? 80 : 410;
        let hudY = IS_TOUCH ? 130 : 170;

        // @ts-ignore
        this.add.text(hudX, hudY, "null", {
            fontSize: '10',
            backgroundColor: '#000000',
            fill: '#FFFF00'
        });

        super(sceneObject.physics.scene, hudX, hudY, "null", {
            fontSize: '10',
            backgroundColor: '#000000',
            fill: '#FFFF00'
        });


        this.setScrollFactor(0);
        sceneObject.physics.scene.add.existing(this);
    }

    update(text = "") {

    }

}
