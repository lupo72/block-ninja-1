/// <reference path="phaser.d.ts"/>
import 'phaser';
import {PreloadScene} from "./scenes/PreloadScene";
import {IntroScene} from "./scenes/IntroScene";
import {GameScene} from "./scenes/GameScene";
import {FinalScene} from "./scenes/FinalScene";

let tE = false;
try {
    const tE = document.createEvent("TouchEvent");
} catch (ex) {
}
// export const IS_TOUCH = (typeof tE === "object");
export const IS_TOUCH = false;

// Landscape
let gameWidth:number = IS_TOUCH ? 580 : window.innerWidth;
let gameHeight:number = IS_TOUCH ? 260 : window.innerHeight;

// console.log(IS_TOUCH, gameWidth, gameHeight);
const config:GameConfig = {
    parent: 'game',
    backgroundColor: "0x000",
    type: Phaser.AUTO,
    width: gameWidth,
    height: gameHeight,
    pixelArt: true,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 480},
            debug: false
        }
    },
    // scale: {
    //     mode: Phaser.Scale.FIT,
    //     autoCenter: Phaser.Scale.CENTER_BOTH,
    // },
    scene: [PreloadScene, IntroScene, GameScene, FinalScene],
};

export const BRICK_TILES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
export const DOOR_LOCK_TILE = 31;
export const LADDER_TILES = [34, 35, 36];
export const TILE_SIZE = {width: 32, height: 32};
export const VULNERABLE_AFTER_HURT = 500;
export const HEALTH_STATES = {max:1000, yellow:700, red: 300};


export function playerInitState() {
    return {
        numKeys: 0,
        walkX: 120,
        runX: 240,
        speedY: 90,
        jumpHeight: -250,
        health: 800,
        lastHit: new Date().getTime()
    };
};

const game = new Phaser.Game(config);
