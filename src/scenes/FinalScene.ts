export class FinalScene extends Phaser.Scene {
    input;
    constructor() {
        super({key: "FinalScene"});
    }

    create() {
        this.add.text(100, 80, "Congratulations Comrade! You survived the haunted woods.");

        this.input.keyboard.on('keydown', function (key) {
            if (key.keyCode === 32) {
                this.scene.stop('FinalScene');
                this.scene.start("IntroScene");
            }
        }, this);
    }
}

