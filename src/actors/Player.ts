import {BRICK_TILES, DOOR_LOCK_TILE, LADDER_TILES, playerInitState, TILE_SIZE, VULNERABLE_AFTER_HURT} from "../index";
import {HealthBar} from "../objects/HealthBar";

export class Player extends Phaser.Physics.Arcade.Sprite {
    physics;
    layer;
    walkX;
    runX;
    speedY;
    jumpHeight;
    ladderTiles;
    attrs;
    healthBar;
    vjoy;
    joystick;
    cursors;

    constructor(sceneObject, x, y, texture, frame, layer, attrs = playerInitState()) {
        super(sceneObject.physics.scene, x, y, texture, frame);
        // this.name = 'player';
        this.scene = sceneObject.scene;
        this.physics = sceneObject.physics;
        this.physics.add.existing(this);
        this.physics.scene.add.existing(this);
        this.layer = layer;
        this.setCollideWorldBounds(true);
        // Adjust Hit-Area
        this.body.setSize(26, 26);
        // Scale Figure
        this.setScale(0.75);
        this.walkX = attrs.walkX;
        this.runX = attrs.runX;
        this.speedY = attrs.speedY;
        this.jumpHeight = attrs.jumpHeight;
        this.ladderTiles = LADDER_TILES;
        this.attrs = attrs;
        this.healthBar = new HealthBar(sceneObject);
        this.healthBar.update(this.attrs.health);
        this.vjoy = null;
        this.joystick = {left: false, right: false, up: false, down: false};
        // this.joystickLeft = false;
        // this.joystickRight = false;
        // this.joystickUp = false;
        // this.joystickDown = false;
    }

    hitKey(player, key) {
        this.attrs.numKeys += 1;
        key.destroy();
    }

    update() {

        let moving = false;
        let running = false;
        let velX = 0;
        let velY = 0;
        let onLadder = this.onLadder();
        let grabLadder = this.grabsLadder();
        let inTheAir = onLadder ? false : this.layer.getTileAtWorldXY(this.body.x, this.body.y) === null;
        this.body.setAllowGravity(inTheAir);
        if (this.cursors.space.isDown) running = true;

        let leftKeyDown = this.joystick.left || this.cursors.left.isDown;
        let rightKeyDown = this.joystick.right || this.cursors.right.isDown;
        let upKeyDown = this.joystick.up || this.cursors.up.isDown;
        let downKeyDown = this.joystick.down || this.cursors.down.isDown;

        if (leftKeyDown) {
            velX = running ? -this.runX : -this.walkX;
            this.flipX = true;
            moving = true;
        } else if (rightKeyDown) {
            velX = running ? this.runX : this.walkX;
            this.flipX = false;
            moving = true;
        }

        let canJump = (this.body.blocked.down || this.body.touching.down) || (onLadder && !grabLadder);

        if (upKeyDown && canJump) {
            this.body.setVelocityY(this.jumpHeight);
            onLadder = false;
        }

        if (upKeyDown && (onLadder || grabLadder)) {
            velY = -this.speedY;
            moving = true;
        }

        if (downKeyDown && (onLadder || grabLadder)) {
            velY = this.speedY;
            moving = true;
        }

        let moveDir = {
            leftKeyDown: leftKeyDown,
            rightKeyDown: rightKeyDown,
            upKeyDown: upKeyDown,
            downKeyDown: downKeyDown
        };

        moving ? this.move(onLadder, grabLadder, velX, velY, running, moveDir) : this.idle(onLadder);
        // tint for player is set in reduceHealth
        // @ts-ignore
        if (this.isTinted && this.attrs.lastHit !== null && this.attrs.lastHit < new Date().getTime()) {
            this.clearTint();
            // @ts-ignore
            this.scene.remove('Sprechblase');
        }

    }

    idle(onLadder) {
        this.body.setVelocityX(0);
        if (onLadder) this.body.setVelocityY(0);
        onLadder ? this.anims.play('Player-Climb-Idle', true) : this.anims.play('Player-Idle', true);
        if (this.onDoorLock()) this.removeDoorLock(0, this.body.height);

    }

    move(onLadder, grabLadder, velX, velY, running, moveDir) {

        let leftKeyDown = moveDir.leftKeyDown;
        let rightKeyDown = moveDir.rightKeyDown;
        let upKeyDown = moveDir.upKeyDown;
        let downKeyDown = moveDir.downKeyDown;

        if (this.body.onWall()) {
            if (leftKeyDown && this.touchingDoorLockLeft()) this.removeDoorLock(-this.body.width);
            if (rightKeyDown && this.touchingDoorLockRight()) this.removeDoorLock(this.body.width);
            // -- Fix prevents not being able to walk ladder in brick path,
            // otherwise figure "falls" 1px and collides with floor tiles
            let tileAbove = this.layer.getTileAtWorldXY(this.body.x, this.body.y - 1);
            let tileAside = null;
            if (leftKeyDown)
                tileAside = this.layer.getTileAtWorldXY(this.body.x - this.body.width, this.body.y);
            else if (rightKeyDown)
                tileAside = this.layer.getTileAtWorldXY(this.body.x + this.body.width, this.body.y);
            if (onLadder && (!tileAside || tileAside && BRICK_TILES.indexOf(tileAside.index) > -1)) this.body.y--;
            if (tileAbove === null) this.body.y--;
        }

        this.body.setVelocityX(velX);

        if (onLadder || grabLadder) {
            this.body.setVelocityY(velY);
            this.anims.play('Player-Climb', true);
        } else this.anims.play(running ? 'Player-Run' : 'Player-Walk', true);
    }

    grabsLadder() {
        let grabLadder = false;
        let tileAtPos = this.layer.getTileAtWorldXY(this.body.x, this.body.y);
        if (tileAtPos) grabLadder = this.ladderTiles.indexOf(tileAtPos.index) > -1;
        return grabLadder;
    }

    onLadder() {
        let onLadder = false;
        let tileBelow = this.layer.getTileAtWorldXY(this.body.x, this.body.y + this.body.height);
        if (tileBelow) onLadder = this.ladderTiles.indexOf(tileBelow.index) > -1;
        return onLadder;
    }

    onDoorLock() {
        let onDoor = false;
        let tileBelow = this.layer.getTileAtWorldXY(this.body.x, this.body.y + this.body.height);
        let doorTiles = [DOOR_LOCK_TILE];
        if (tileBelow) onDoor = doorTiles.indexOf(tileBelow.index) > -1;
        return onDoor;
    }

    touchingDoorLockLeft() {
        let retVal = false;
        let nextTile = this.layer.getTileAtWorldXY(this.body.x - this.body.width, this.body.y);
        if (nextTile) {
            if (parseInt(nextTile.index) === DOOR_LOCK_TILE && this.attrs.numKeys > 0) retVal = true;
        }
        return retVal;
    }

    touchingDoorLockRight() {
        let retVal = false;
        let nextTile = this.layer.getTileAtWorldXY(this.body.x + this.body.width, this.body.y);
        if (nextTile) {
            if (parseInt(nextTile.index) === DOOR_LOCK_TILE && this.attrs.numKeys > 0)
                retVal = true;
        }
        return retVal;
    }

    removeDoorLock(addOrSubX = 0, addOrSubY = 0) {
        if (this.attrs.numKeys === 0) return;
        this.attrs.numKeys--;
        let tileX = Math.round((this.body.x + addOrSubX) / TILE_SIZE.width);
        let tileY = Math.round((this.body.y + addOrSubY) / TILE_SIZE.height);
        this.layer.removeTileAt(tileX, tileY);
    }

    reduceHealth(amount) {
        console.log('reduceHealth:' + amount);
        this.attrs.health -= amount;
        this.attrs.lastHit = new Date().getTime() + VULNERABLE_AFTER_HURT;
        // @ts-ignore
        this.setTintFill(0xaa0000, 0xbb0000, 0xcc0000, 0xdd0000, 0xee0000);
        this.healthBar.update(this.attrs.health);
        if (this.attrs.health <= 0) {
            this.attrs = playerInitState();
            // @ts-ignore
            this.scene.stop('GameScene');
            // @ts-ignore
            this.scene.remove('Sprechblase');
            // @ts-ignore
            this.scene.pause('SoundScene');
            // @ts-ignore
            this.scene.start('Intro');
        }
    }
}
