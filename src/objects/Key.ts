export class Key extends Phaser.GameObjects.Sprite {

    constructor(gameScene, objekt, player, map) {

        var x = objekt.x;
        var y = objekt.y;
        var texture = 'Tiles';
        var frame = objekt.gid - 1;
        super(gameScene.physics.scene, x, y, texture, frame);
        gameScene.physics.scene.add.existing(this);
        gameScene.physics.add.existing(this);
        this.body.setAllowGravity(false);
        this.body.setImmovable(true);
        gameScene.physics.add.overlap(player, this, player.hitKey, null, player);
    }
}
