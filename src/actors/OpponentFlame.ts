// https://rexrainbow.github.io/phaser3-rex-notes/docs/site/tilesprite/index.html#custom-class
import {BaseOpponent} from "./BaseOpponent";

export class OpponentFlame extends BaseOpponent {

    constructor(game, objekt, layer, world) {

        var x = objekt.x;
        var y = objekt.y;
        var texture = 'Tiles';
        var frame = objekt.gid - 1;

        super(game.physics.scene, x, y, texture, frame);
        this.player = game.player;
        this.physics = game.physics;
        this.physics.add.existing(this);
        this.physics.scene.add.existing(this);
        this.setCollideWorldBounds(true);
        this.layer = layer;
        this.scene = game.scene;
        this.tileSize = {tileWidth: world.tileWidth, tileHeight: world.tileHeight};
        this.body.setSize(28, 24);
        this.anims.play('Opponent-Flame-Walk');
        this.velX = Math.ceil((Math.random() + 10) * 12);
        this.game = game;
        this.damage = objekt.hasOwnProperty('damage') ? objekt.damage : 500;
    }

    update() {
        var xAhead = this.velX > 0 ? this.body.x + this.tileSize.tileWidth : this.body.x;
        var yBelow = this.body.y + this.tileSize.tileHeight;
        var nextFloorTile = this.layer.getTileAtWorldXY(xAhead, yBelow);
        if (this.velX > 0 && this.body.blocked.right || this.velX < 0 && this.body.blocked.left || nextFloorTile === null) {
            this.flipX = !this.flipX;
            this.velX = -this.velX;
        }
        this.body.setVelocityX(this.velX);
        this.physics.world.collide(this, this.player, this.game.hitEvent, null, this);
    }

}
