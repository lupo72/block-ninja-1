// import PlayerState from "../PlayerState";

import {Sound} from "../objects/Sound";
import {IS_TOUCH} from "../index";
import {Sprechblase} from "./Sprechblase";
import {OpponentFlame} from "../actors/OpponentFlame";
import {Player} from "../actors/Player";
import {MovingPlatform} from "../objects/MovingPlatform";
import {Key} from "../objects/Key";

export class GameScene extends Phaser.Scene {
    gameAttrs;
    level;
    worldmap;
    sound;
    minimap;
    parallax1;
    parallax2;
    parallax3;
    collisionLayer;
    player;
    gameObjects;
    headsUpDisplay;
    hudText;

    constructor() {
        super({key: "GameScene"});
    }

    create(attrs) {
        if (attrs && attrs.hasOwnProperty('level')) {
            this.gameAttrs = attrs;
            this.level = attrs.level;
            this.worldmap = this.add.tilemap('Level' + this.level);
        } else {
            this.worldmap = this.add.tilemap('Level1');
        }

        if (!this.scene.get('SoundScene')) {
            this.sound = this.game.scene.add('Sound', Sound, true, {x: 0, y: 0});
        } else {
            this.scene.resume('SoundScene');
        }
        // this.miniMap = new MiniMap(this, {}).init();
        // this.miniMap.backgroundColor.setTo(128,255,255);

        this.minimap = this.cameras.add(10, 10, 100, 50).setZoom(0.1).setName('mini');
        // this.miniMap.setBackgroundColor(0x002244);
        this.minimap.setBackgroundColor(0xefefef);
        this.minimap.scrollX = 0;
        this.minimap.scrollY = 0;

        this.parallax1 = this.add.tileSprite(0, 0,this.game.config.width, this.game.config.height, 'back');
        this.parallax2 = this.add.tileSprite(0, 0, this.game.config.width, this.game.config.height, 'middle');
        this.parallax3 = this.add.tileSprite(0, 0, this.game.config.width, this.game.config.height, 'front');
        this.parallax1.setOrigin(0, 0).setScrollFactor(0);
        this.parallax2.setOrigin(0, 0).setScrollFactor(0);
        this.parallax3.setOrigin(0, 0).setScrollFactor(0);

        let minimapIgnoreContent = this.add.container(0,0);
        minimapIgnoreContent.add([this.parallax1, this.parallax2, this.parallax3]);

        const tiles = this.worldmap.addTilesetImage('Tiles');
        this.collisionLayer = this.worldmap.createDynamicLayer('Welt', tiles, 0, 0);
        // Bricks, etc.
        this.collisionLayer.setCollisionBetween(0, 18);
        // Door Tile
        this.collisionLayer.setCollision([30, 31]);
        // Ladder Tiles
        this.collisionLayer.setCollisionBetween(32, 35, false);
        // Door: change Level
        this.collisionLayer.setTileIndexCallback([19], this.changeLevel, this);

        this.player = new Player(this, 320, 240, 'Player', 1, this.collisionLayer, attrs.playerAttrs);
        this.player.cursors = this.input.keyboard.createCursorKeys();
        // if (IS_TOUCH) {
        //     this.player.vjoy = this.plugins.get('rexvirtualjoystickplugin').add(this, {
        //         x: this.cameras.main.width / 2,
        //         y: this.cameras.main.height / 2 + 40,
        //         radius: 100,
        //         base: this.add.circle(0, 0, 100, 0x888888).setAlpha(.5),
        //         thumb: this.add.circle(0, 0, 50, 0xcccccc).setAlpha(.3),
        //         // dir: '8dir',
        //         // 'up&down'|0|'left&right'|1|'4dir'|2|'8dir'|3
        //         // forceMin: 16,
        //         // enable: true
        //     }).on('update', this.setJoystickState, this);
        //     minimapIgnoreContent.add([this.player.vjoy.base, this.player.vjoy.thumb]);
        // } //./IS_TOUCH


        // Set Objects not to be rendered by MiniMap Camera
        this.minimap.ignore(minimapIgnoreContent);

        this.player.groundLayer = this.collisionLayer;
        this.player.time = this.time;
        this.gameObjects = new Phaser.GameObjects.Group(this);
        const objekte = this.worldmap.getObjectLayer('Objekte');
        if (objekte) {
            for (var i = 0; i < objekte.objects.length; i++) {
                let objekt = objekte.objects[i];
                let sprite;

                switch (objekt.type) {
                    case 'moving-platform':
                        sprite = new MovingPlatform(this.physics.scene, objekt, this.player, this.worldmap);
                        break;
                    // case 'enemy':
                    case 'fire':
                        sprite = new OpponentFlame(this, objekt, this.collisionLayer, this.worldmap);
                        this.physics.add.collider(this.collisionLayer, sprite);
                        break;
                    case 'key':
                        sprite = new Key(this.physics.scene, objekt, this.player, this.worldmap);
                        sprite.setOrigin(.5, .5);
                        this.physics.add.existing(sprite);
                        this.physics.add.collider(this.collisionLayer, sprite);
                        break;
                    case 'startpunkt':
                        this.player.x = objekt.x;
                        this.player.y = objekt.y;
                        break;
                }
                if (sprite) this.gameObjects.add(sprite);
            }
        }
        // -- Add Collision Detection for defined Tiles within Layer and Player
        this.physics.add.collider(this.collisionLayer, this.player);
        this.cameras.main.startFollow(this.player);
        // this.miniMap.startFollow(this.player);
        this.minimap.setBounds(0, 0, this.worldmap.widthInPixels, this.worldmap.heightInPixels);

        // @ts-ignore
        this.cameras.main.centerOn(this.player.x, this.player.y);
        this.cameras.main.setZoom(2);
        this.cameras.main.setBackgroundColor('0xccbbdd');
        this.cameras.main.setBounds(0, 0, this.worldmap.widthInPixels, this.worldmap.heightInPixels);
        // Set this Bounds manually (i guess for scrolling maps)!
        this.physics.world.bounds.width = this.collisionLayer.width;
        this.physics.world.bounds.height = this.collisionLayer.height;

        this.hudText = this.add.text(0, 0, null, {
            fontSize: '10',
            backgroundColor: '#000000',
            fill: '#FFFF00'
        });
        let hudX = IS_TOUCH ? 205 : 310;
        let hudY = IS_TOUCH ? 70 : 158;
        this.headsUpDisplay = this.add.container(hudX, hudY, [this.hudText, this.player.healthBar]);
        this.headsUpDisplay.setScrollFactor(0);
    }

    changeLevel(target) {

        if (target.constructor.name !== "Player") return;

        this.scene.stop('GameScene');
        this.level += 1;
        this.gameAttrs.playerAttrs = this.player.attrs;
        this.gameAttrs.level = this.level;
        if (this.level > 5) {
            this.scene.start('FinalScene');
            this.scene.pause('Sound');
        } else {
            this.scene.start('GameScene', this.gameAttrs);
        }
    }

    update(time, delta) {
        this.backgroundAnimation();

        if (this.gameObjects) {
            this.gameObjects.children.iterate(function (child) {
                child.update();
            });
        }
        if (this.player) {
            this.player.update();
            this.hudText.text = this.getHudText();
        }
    }

    getHudText() {
        return "Level: " + this.gameAttrs.level + " Keys: " + this.player.attrs.numKeys;
    }

    backgroundAnimation() {
        // Parallax Effect: https://www.youtube.com/watch?v=pknZUn82x2U
        this.parallax1.tilePositionX = this.cameras.main.scrollX * .2;
        this.parallax2.tilePositionX = this.cameras.main.scrollX * .4;
        this.parallax3.tilePositionX = this.cameras.main.scrollX * .6;

        this.parallax1.tilePositionY = this.cameras.main.scrollY * .2 + 140;
        this.parallax2.tilePositionY = this.cameras.main.scrollY * .4 + 160;
        this.parallax3.tilePositionY = this.cameras.main.scrollY * .6 + 180;
    }

    hitEvent(opponent, player) {
        let quotes = ['Ouch', 'that hurt!', 'F**ck', 'Sh*t', 'Stop It!', 'Don\'t do that!', 'Dammit'];
        let text = quotes[Math.ceil(Math.random() * quotes.length - 1)];
        if (player.attrs.lastHit < (new Date().getTime())) {
            player.reduceHealth(opponent.damage);

            if (!this.scene.get('Sprechblase')) {
                // @ts-ignore
                this.scene.add('Sprechblase', Sprechblase, true, {
                    x: 0,
                    y: 0,
                    text: text
                });

                // Activate This Part when moving on to RPG
                // folllowing fixes Issue "player keeps walking" in desktop after resuming GameScene
                // this.player.cursors.left.isDown = false;
                // this.player.cursors.right.isDown = false;
                // this.player.cursors.up.isDown = false;
                // this.player.cursors.down.isDown = false;
                // this.scene.pause('GameScene');
            }

        }
    }

    setJoystickState() {
        let cursorKeys = this.player.vjoy.createCursorKeys();
        this.player.joystick.left = false;
        this.player.joystick.right = false;
        this.player.joystick.up = false;
        this.player.joystick.down = false;

        this.player.joystick.left = cursorKeys['left'].isDown;
        this.player.joystick.right = cursorKeys['right'].isDown;
        this.player.joystick.up = cursorKeys['up'].isDown;
        this.player.joystick.down = cursorKeys['down'].isDown;
    }

}
