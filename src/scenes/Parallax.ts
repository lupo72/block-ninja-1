export class Parallax extends Phaser.GameObjects.TileSprite {
    parallax1;
    parallax2;
    parallax3;

    constructor() {
        super({key: "Parallax"});
    }


    create() {

        this.parallax1 = this.add.tileSprite(0, 0, this.game.config.width, this.game.config.height, 'back');
        this.parallax2 = this.add.tileSprite(0, 0, this.game.config.width, this.game.config.height, 'middle');
        this.parallax3 = this.add.tileSprite(0, 0, this.game.config.width, this.game.config.height, 'front');
        this.parallax1.setOrigin(0, 0).setScrollFactor(0);
        this.parallax2.setOrigin(0, 0).setScrollFactor(0);
        this.parallax3.setOrigin(0, 0).setScrollFactor(0);

    }

    update(time: number, delta: number) {
                // Parallax Effect: https://www.youtube.com/watch?v=pknZUn82x2U
        this.parallax1.tilePositionX = this.cameras.main.scrollX * .2;
        this.parallax2.tilePositionX = this.cameras.main.scrollX * .4;
        this.parallax3.tilePositionX = this.cameras.main.scrollX * .6;

        this.parallax1.tilePositionY = this.cameras.main.scrollY * .2 + 140;
        this.parallax2.tilePositionY = this.cameras.main.scrollY * .4 + 160;
        this.parallax3.tilePositionY = this.cameras.main.scrollY * .6 + 180;

    }


}
