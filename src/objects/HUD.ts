import {HealthBar} from "./HealthBar";
import {PlayerText} from "./PlayerText";

export class HUD extends Phaser.GameObjects.GameObject
{
    playerText;
    healthBar;
    constructor(scene) {
        super(scene, 'container');

        this.playerText = new PlayerText(scene);
        this.healthBar = new HealthBar(scene);
    }
}
