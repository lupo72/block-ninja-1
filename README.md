# 🎮 The Block Ninja  
 
 _Old-school Fun with Phaser3_
 
#### What is Phaser?
>"Phaser is an HTML5 game framework which aims to help developers make powerful, cross-browser HTML5 games really quickly. It was created specifically to harness the benefits of modern browsers, both desktop and mobile. The only browser requirement is the support of the canvas tag."

### Dependencies

[Node.js + NPM](https://nodejs.org/)

Install the dependencies and devDependencies and start the dev-server.

```sh
$ cd phaser3-typescript-boilerplate
$ npm install
$ npm run dev
```

For production environments...

```sh
$ npm install
$ npm run build
```

### Where can i start to learn about Phaser 3?
You could follow the [official tutorial](http://phaser.io/tutorials/making-your-first-phaser-3-game) that make a simple plataform game

Notes and useful links:

https://www.html5gamedevs.com/topic/36632-changing-collision-detection-for-arcade-physics/

