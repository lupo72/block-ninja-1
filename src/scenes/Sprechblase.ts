import {IS_TOUCH} from "../index";

export class Sprechblase extends Phaser.Scene {
    constructor() {
        super({key: "Sprechblase"});
    }

    create(attrs) {
        let width = this.cameras.main.width;
        let height = this.cameras.main.height;
        let box = this.add.graphics();
        box.fillStyle(0x222222, 0.8);
        let boxX = IS_TOUCH ? 20 : 240;
        let boxY = IS_TOUCH ? (height / 2) - 20 : 270;
        let boxW = IS_TOUCH ? width - 40 : 320;
        let boxH = 50;
        box.fillRect(boxX, boxY, boxW, boxH);
        let label = this.make.text({
            x: width / 2,
            y: height / 2,
            text: attrs.text,
            style: {
                font: '20px monospace',
                fill: '#ffffff'
            }
        });
        label.setOrigin(0.5, 0.5);

        // Activate This Part when moving on to RPG
        // (maybe i'll use something like this as Template for "In Game Fight Scene" later)
        // https://github.com/photonstorm/phaser/issues/4194
        // box.setInteractive(new Phaser.Geom.Rectangle(20, (height / 2) - 20, width - 40, 50), Phaser.Geom.Rectangle.Contains);
        // box.on('pointerdown', function(){
        //     this.scene.resume('GameScene');
        //     this.scene.remove('Sprechblase');
        // }, this);
    }
}


