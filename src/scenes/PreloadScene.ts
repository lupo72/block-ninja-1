export class PreloadScene extends Phaser.Scene {

    constructor() {
        super({key: "PreloadScene"});
    }

    preload() {
        this.load.path = './src/';
        // https://gamedevacademy.org/creating-a-preloading-screen-in-phaser-3/?a=13
        let width = this.cameras.main.width;
        let height = this.cameras.main.height;

        let progressBar = this.add.graphics();
        let progressBox = this.add.graphics();
        progressBox.fillStyle(0x222222, 0.8);
        progressBox.fillRect(20, height / 2 - 70, width - 60, 50);

        let loadingText = this.make.text({
            x: width / 2,
            y: height / 2 - 50,
            text: 'Loading...',
            style: {
                font: '20px monospace',
                fill: '#ffffff'
            }
        });
        loadingText.setOrigin(0.5, 0.5);
        let percentText = this.make.text({
            x: width / 2,
            y: height / 2 - 5,
            text: '0%',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });
        percentText.setOrigin(0.5, 0.5);

        let assetText = this.make.text({
            x: width / 2,
            y: height / 2 + 50,
            text: '',
            style: {
                font: '18px monospace',
                fill: '#ffffff'
            }
        });
        assetText.setOrigin(0.5, 0.5);


        this.load.on('progress', function (value) {
            // console.log(value);
            progressBar.clear();
            progressBar.fillStyle(0xffffff, 1);
            progressBar.fillRect(20 , height / 2 - 60, (width - 60) * value, 30);
            percentText.setText((value * 100) + '%');
        });

        this.load.on('fileprogress', function (file) {
            // console.log(file.src);
            assetText.setText('Loading asset: ' + file.key);
        });

        this.load.on('complete', function () {
            // console.log('complete');
            progressBar.destroy();
            progressBox.destroy();
            loadingText.destroy();
            percentText.destroy();
            assetText.destroy();
        });

        // Original Source 'https://raw.githubusercontent.com/rexrainbow/phaser3-rex-notes/master/plugins/dist/rexvirtualjoystickplugin.min.js';
        // let url = 'assets/plugins/rexvirtualjoystickplugin.min.js';
        // this.load.plugin('rexvirtualjoystickplugin', url, true);

        this.load.tilemapTiledJSON('Level1', 'assets/tilemaps-json/level1.json');
        this.load.tilemapTiledJSON('Level2', 'assets/tilemaps-json/level2.json');
        this.load.tilemapTiledJSON('Level3', 'assets/tilemaps-json/level3.json');
        this.load.tilemapTiledJSON('Level4', 'assets/tilemaps-json/level4.json');
        this.load.tilemapTiledJSON('Level5', 'assets/tilemaps-json/level5.json');
        this.load.spritesheet('Tiles', 'assets/tilesets/Tiles-2-jungle-theme.png', {frameWidth: 32, frameHeight: 32});
        this.load.spritesheet('Player', 'assets/tilesets/block-ninja-32px.png', {frameWidth: 32, frameHeight: 32});
        this.load.spritesheet('Opponent-Flame', 'assets/tilesets/fireball.png', {frameWidth: 32, frameHeight: 32});

        // Loading Parallax Background Images
        // https://opengameart.org/content/forest-parallax
        this.load.image('back', './assets/bg/Back.png');
        this.load.image('middle', './assets/bg/middle.png');
        this.load.image('front', './assets/bg/front.png');
        this.load.image('IntroBg', './assets/bg/level0.png');

        this.load.audio('Soundtrack', ['assets/audio/Vulnerability - Final No Echo.ogg']);
        this.load.image('oldschool', 'assets/fonts/charmap-oldschool_white.png');
        this.load.image('BlockFont', 'assets/fonts/Block Font.png');
        // https://opengameart.org/content/pixel-block-font
    }

    create() {
        this.scene.stop('PreloadScene');
        this.scene.start('IntroScene');
    }
}
