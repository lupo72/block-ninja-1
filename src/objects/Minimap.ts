
// let attrs = {
//     scene,
//     x = 10,﻿
//     y = 10,
//     width = 192,
//     height = 192,
//     zoom = 0.1,
//     scroll = { x: 960, y: 960 }
//   };

export class MiniMap extends Phaser.Cameras.Scene2D.Camera {
    scroll;

    constructor(scene, attrs) {
        let x = 10;
        let y = 10;
        let width = 192;
        let height = 192;
        let zoom = 0.1;
        let scroll = { x: 960, y: 960};
        super(x, y, width, height);
        this.scene = scene;
        this.zoom = zoom;
        this.scroll = scroll;
        // this.ignore([scene.parallax1, scene.parallax2, scene.parallax3]);
    }

    init() {
        this.scene.cameras.cameras.push(this);
        this.setZoom(this.zoom);
        this.setScroll(this.scroll.x, this.scroll.y);
        return this;
    }
}


