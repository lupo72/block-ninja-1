export class Sound extends Phaser.Scene {
    music;
    constructor() {
        super({key: "SoundScene"});
    }
    create() {
        this.events.on('pause', function(){
            this.music.stop();
        }, this);

        this.events.on('resume', function(){
            this.music.play();
        }, this);

        this.music = this.sound.add('Soundtrack',{loop:true});

        if ( ! this.music.isPlaying )
        {
            this.music.play();
            this.music.volume = 0.5;
        }
    }
}
